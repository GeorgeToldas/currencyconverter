package com.georgetoldas.currencyconverter

import android.app.Activity
import android.app.Application
import com.georgetoldas.currencyconverter.di.AppComponent
import com.georgetoldas.currencyconverter.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class CurrencyApp : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .application(this)
            .apiAddress(BuildConfig.API_ENDPOINT)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }

    override fun activityInjector() = activityInjector
}