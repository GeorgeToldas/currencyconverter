package com.georgetoldas.currencyconverter.main

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.georgetoldas.currencyconverter.R
import com.georgetoldas.currencyconverter.main.adapter.CurrencyAdapter
import com.georgetoldas.currencyconverter.utils.CurrencyPrefs
import com.georgetoldas.currencyconverter.utils.KeyboardEventListener
import com.georgetoldas.currencyconverter.utils.hideKeyboard
import com.georgetoldas.currencyconverter.utils.observeNonNull
import com.georgetoldas.resourceprovider.CurrencyResourceProviderImpl
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.layout_activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private lateinit var currencyAdapter: CurrencyAdapter
    private lateinit var snackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_main)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        setupAdapter()
        observeCurrencyLiveData()
    }

    private fun setupAdapter() {
        currencyAdapter = CurrencyAdapter(
            baseCurrency = CurrencyPrefs(this).baseValue,
            currencyResourceProvider = CurrencyResourceProviderImpl(this),
            onItemSwapped = { baseCurrency, currencyList ->
                // scroll to top, swap rows in DB
                currencyRecyclerView.scrollToPosition(0)
                viewModel.swapRow(baseCurrency, currencyList)
            },
            onItemClicked = { position ->
                currencyAdapter.swapRow(position)
            },
            onInputUpdated = {
                // Update locally then notify DB
                currencyRecyclerView.post {
                    currencyAdapter.updateValues(it)
                }
                viewModel.updateValues(it)
            })
        currencyRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            (itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
            adapter = currencyAdapter
        }
    }

    private fun observeCurrencyLiveData() {
        viewModel.getCurrencyLiveData().observeNonNull(this) {
            currencyAdapter.updateCurrencies(it)
        }
        viewModel.getKeyboardOpenLiveData().observeNonNull(this) {
            currencyAdapter.setIsKeyboardOpen(it)
        }
        viewModel.getConnectivityLiveData().observeNonNull(this) { isNetworkAvailable ->
            if (isNetworkAvailable) {
                if(::snackbar.isInitialized) {
                    snackbar.dismiss()
                    showSnackbar(rootLayout, R.string.online_notice, Snackbar.LENGTH_LONG) {}
                }
                viewModel.getNetworkCurrencies()
            } else {
                viewModel.getCurrenciesOffline()
                showSnackbar(rootLayout, R.string.offline_notice, Snackbar.LENGTH_INDEFINITE) {
                    Snackbar.make(rootLayout, R.string.message_dismissed, Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun showSnackbar(
        view: View,
        @StringRes text: Int,
        duration: Int,
        callback: () -> Unit
    ) {
        snackbar = Snackbar.make(view, text, duration)
            .setAction(R.string.dismiss) {
                callback()

            }
        snackbar.show()
    }

    override fun onResume() {
        super.onResume()
        KeyboardEventListener(this) { isOpen ->
            currencyAdapter.setIsKeyboardOpen(isOpen)
        }
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }
}