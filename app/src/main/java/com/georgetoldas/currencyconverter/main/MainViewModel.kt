package com.georgetoldas.currencyconverter.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.georgetoldas.room.Currency
import com.georgetoldas.room.CurrencyDb
import com.georgetoldas.currencyconverter.utils.CurrencyPrefs
import com.georgetoldas.network.ApiInterface
import com.georgetoldas.network.connection.ConnectivityLiveData
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    private val currencyDb: CurrencyDb,
    private val apiInterface: ApiInterface,
    private val currencyPrefs: CurrencyPrefs,
    private val connectivityLiveData: ConnectivityLiveData
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val currencyLiveData = MutableLiveData<LinkedList<Currency>>()
    private val keyboardOpenLiveData = MutableLiveData<Boolean>()

    private var isUpdatingDb = false

    fun getCurrencyLiveData() = currencyLiveData
    fun getKeyboardOpenLiveData() = keyboardOpenLiveData
    fun getConnectivityLiveData() = connectivityLiveData

    private fun getCurrencyList() {
        if (connectivityLiveData.value == true) {
            getNetworkCurrencies()
        } else {
            getCurrenciesOffline()
        }
    }

    fun getNetworkCurrencies() = with(compositeDisposable) {
        clear()
        // repeat API call every second
        add(Observable.interval(1, TimeUnit.SECONDS)
            .concatMap { apiInterface.getCurrencies(currencyPrefs.selectedCurrency) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe(
                { currencies ->
                    addOrUpdateCurrencies(currencies.rates, currencyPrefs.selectedCurrency)
                },
                { Log.d(TAG, it.toString()) })
        )
    }

    fun getCurrenciesOffline() = with(compositeDisposable) {
        clear()
        add(
            Observable.just(currencyDb.currencyDao())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({ currencyDao ->
                    with(currencyDao) {
                        // Notify the live data
                        if (currencyPrefs.baseValue != 0f) {
                            updateRatesOffline(currencyPrefs.baseValue)
                        }
                        postUpdatedList(getCurrencies()) {
                            currencyLiveData.postValue(it)
                        }
                        isUpdatingDb = false
                    }
                },
                    { Log.d(TAG, it.toString()) })
        )
    }

    private fun addOrUpdateCurrencies(
        currencies: HashMap<String, Float>?,
        selectedCurrency: String
    ) =
        with(currencyDb.currencyDao()) {
            if (getCurrencies().isEmpty()) {
                // Populate DB on initial start, save insertion order
                var order = 0
                insert(convertToDao(selectedCurrency, BASE_VALUE, order++))
                currencyPrefs.baseValue = BASE_VALUE
                currencies?.forEach {
                    insert(convertToDao(it.key, it.value, order++))
                }
            } else {
                // Update rates
                currencies?.forEach {
                    updateRates(it.key, it.value, currencyPrefs.baseValue * it.value)
                }
            }
            // Notify the live data
            postUpdatedList(getCurrencies()) {
                currencyLiveData.postValue(it)
            }
        }


    private fun convertToDao(name: String, value: Float, order: Int) =
        Currency(name, value, value, order)

    fun swapRow(baseValue: Float, currencies: LinkedList<Currency>) = with(compositeDisposable) {
        // stop API calls while performing DB operation
        clear()
        add(
            Observable.just(currencyDb.currencyDao())
                .debounce(50, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    with(it) {
                        if (isDisposed) return@subscribe
                        isUpdatingDb = true
                        // update order in DB
                        currencies.map { currency ->
                            currency.order = currencies.indexOf(currency)
                        }
                        updateList(currencies)
                        postUpdatedList(currencies) { updatedList ->
                            // Save selected currency
                            currencyPrefs.baseValue = baseValue
                            currencyPrefs.selectedCurrency = updatedList.elementAt(0).name
                            // resume updating the UI with network data
                            getCurrencyList()
                        }
                    }
                }, {
                    Log.d(TAG, it.toString())
                })
        )
    }

    fun updateValues(baseValue: Float) = with(compositeDisposable) {
        clear()
        add(
            Observable.just(currencyDb.currencyDao())
                .debounce(50, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    with(it) {
                        updateValues(baseValue)
                        postUpdatedList(getCurrencies()) { updatedList ->
                            // Save selected currency
                            currencyPrefs.baseValue = baseValue
                            currencyPrefs.selectedCurrency = updatedList.elementAt(0).name
                            // resume updating the UI with network data
                            getCurrencyList()
                        }
                    }
                }, {
                    Log.d(TAG, it.toString())
                })
        )
    }

    private fun postUpdatedList(
        list: List<Currency>,
        callback: (LinkedList<Currency>) -> Unit
    ) {
        LinkedList<Currency>().apply {
            addAll(list)
        }.also {
            callback(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    companion object {
        private const val BASE_VALUE = 1f
        private val TAG = MainViewModel::class.java.simpleName
    }
}