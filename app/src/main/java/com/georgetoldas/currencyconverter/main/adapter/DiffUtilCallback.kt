package com.georgetoldas.currencyconverter.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.georgetoldas.room.Currency

class CurrencyDiffCallback(
    private val oldCurrencyList: List<Currency>,
    private val newCurrencyList: List<Currency>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldCurrencyList.size

    override fun getNewListSize() = newCurrencyList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldCurrencyList[oldItemPosition].name == newCurrencyList[newItemPosition].name

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldCurrencyList[oldItemPosition].rate.equals(newCurrencyList[newItemPosition].rate)
}