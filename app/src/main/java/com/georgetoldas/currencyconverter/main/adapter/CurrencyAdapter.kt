package com.georgetoldas.currencyconverter.main.adapter

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.georgetoldas.currencyconverter.R
import com.georgetoldas.room.Currency
import com.georgetoldas.currencyconverter.utils.DecimalDigitsInputFilter
import com.georgetoldas.currencyconverter.utils.getCurrencyColor
import com.georgetoldas.currencyconverter.utils.validateTextInput
import com.georgetoldas.resourceprovider.CurrencyResourceProvider
import kotlinx.android.synthetic.main.item_currency.view.*
import java.util.*

class CurrencyAdapter(
    private var baseCurrency: Float,
    private var currencyResourceProvider: CurrencyResourceProvider,
    private var currencyList: LinkedList<Currency> = LinkedList(),
    private val onItemSwapped: (Float, LinkedList<Currency>) -> Unit = { _, _ -> },
    private val onItemClicked: (Int) -> Unit = {},
    private val onInputUpdated: (Float) -> Unit = {}
) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    private var isKeyboardOpen = false
    private var isSwapping = false

    override fun getItemCount() = currencyList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_currency,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(position)
    }

    fun updateCurrencies(list: LinkedList<Currency>) {
        this.currencyList = list
        notifyItemRangeChanged(SECOND_ITEM, itemCount - 1)
        isSwapping = false
    }

    fun updateValues(userInput: Float) {
        baseCurrency = userInput
        notifyItemRangeChanged(SECOND_ITEM, itemCount - 1)
    }

    fun swapRow(currentPos: Int) {
        if (currentPos == FIRST_ITEM) return
        isSwapping = true

        // Push selected currency to top of the list
        val oldList = currencyList
        val selectedCurrency = currencyList[currentPos]
        currencyList.remove(selectedCurrency)
        currencyList.addFirst(selectedCurrency)

        // Set base currency and dispatch list updates
        baseCurrency = selectedCurrency.value
        val diffResult = DiffUtil.calculateDiff(CurrencyDiffCallback(oldList, currencyList), false)
        diffResult.dispatchUpdatesTo(this)
        notifyItemMoved(currentPos, FIRST_ITEM)
        notifyItemChanged(FIRST_ITEM)

        // notify ViewModel with updated list
        onItemSwapped(baseCurrency, currencyList)
    }

    fun setIsKeyboardOpen(isKeyboardOpen: Boolean) {
        this.isKeyboardOpen = isKeyboardOpen
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        if (holder.adapterPosition == FIRST_ITEM) {
            // request focus when top item attaches to view
            with(holder.itemView.currencyEditText) {
                requestFocus()
                text?.let {
                    setSelection(it.length)
                }
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        // clear focus / hide keyboard when detaching top item from view
        if (holder.adapterPosition == FIRST_ITEM) {
            with(holder.itemView.currencyEditText) {
                clearFocus()
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(position: Int) = with(itemView) {
            with(currencyList[position]) {
                val currencyValue: String = if (baseCurrency == 0f) {
                    NO_CURRENCY
                } else {
                    if (position == FIRST_ITEM) {
                        value.toString().validateTextInput(true)
                    } else {
                        (rate * baseCurrency).toString().validateTextInput(false)
                    }
                }

                currencyTextView.text = name
                // set flag resource and full currency name
                with(currencyResourceProvider) {
                    flagImageView.setImageResource(getCurrencyFlag(name))
                    currencyDescTextView.setText(getCurrencyName(name))
                }
                setupCurrencyEditView(currencyValue, position == FIRST_ITEM)
            }
            setListeners()
        }

        private fun setupCurrencyEditView(
            currencyValue: String,
            isFirstItem: Boolean
        ) = with(itemView) {
            with(currencyEditText) {
                filters = arrayOf<InputFilter>()
                setTextColor(currencyValue.getCurrencyColor(context))
                setText(currencyValue)
                if (isFirstItem) {
                    requestFocus()
                    text?.let {
                        setSelection(it.length)
                    }
                }
                filters = arrayOf<InputFilter>(DecimalDigitsInputFilter())
            }
        }

        private fun setListeners() = with(itemView) {
            setOnClickListener {
                onItemClicked(adapterPosition)
            }
            currencyEditText.setOnTouchListener { _, motionEvent ->
                if (motionEvent.action == MotionEvent.ACTION_UP) {
                    onItemClicked(adapterPosition)
                }
                return@setOnTouchListener false
            }
            currencyEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    currencyEditText.removeTextChangedListener(this)
                    // validate input for topmost EditText item
                    if (adapterPosition == FIRST_ITEM && isKeyboardOpen && !isSwapping) {
                        s.toString().let {
                            if (it == currencyList[adapterPosition].value.toString()) {
                                return@let
                            }
                            val validatedInput = it.validateTextInput(true)
                            with(currencyEditText) {
                                setText(validatedInput)
                                setTextColor(validatedInput.getCurrencyColor(context))
                                setSelection(validatedInput.length)
                            }
                            onInputUpdated(validatedInput.toFloat())
                            currencyEditText.addTextChangedListener(this)
                        }
                    }
                }
            })

        }
    }

    companion object {
        const val NO_CURRENCY = "0"
        const val FIRST_ITEM = 0
        const val SECOND_ITEM = 1
    }
}