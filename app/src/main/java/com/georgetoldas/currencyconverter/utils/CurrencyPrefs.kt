package com.georgetoldas.currencyconverter.utils

import android.content.Context

class CurrencyPrefs(context: Context) {

    companion object {
        private const val KEY_BASE_CURRENCY_VALUE = "PREF_BASE_CURRENCY_VALUE"
        private const val KEY_SELECTED_CURRENCY = "PREF_SELECTED_CURRENCY"
        private const val BASE_CURRENCY_VALUE = 1f
        private const val BASE_SELECTED_CURRENCY = "EUR"
    }

    private val sharedPreferences =
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    var baseValue: Float
        get() = sharedPreferences.getFloat(KEY_BASE_CURRENCY_VALUE, BASE_CURRENCY_VALUE)
        set(value) {
            sharedPreferences.edit().putFloat(KEY_BASE_CURRENCY_VALUE, value).apply()
        }

    var selectedCurrency: String
        get() = sharedPreferences.getString(KEY_SELECTED_CURRENCY, BASE_SELECTED_CURRENCY)!!
        set(value) {
            sharedPreferences.edit().putString(KEY_SELECTED_CURRENCY, value).apply()
        }

}