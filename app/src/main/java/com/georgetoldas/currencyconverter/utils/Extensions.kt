package com.georgetoldas.currencyconverter.utils

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.georgetoldas.currencyconverter.R
import java.math.BigDecimal
import kotlin.math.roundToInt

/**
 * Live Data
 */
fun <T> LiveData<T>.observeNonNull(lifecycleOwner: LifecycleOwner, action: (T) -> Unit) =
    observe(lifecycleOwner, Observer { it?.let(action) })

/**
 * Text
 */

fun String.validateTextInput(isFirstItem: Boolean): String {
    val maxInt = if (isFirstItem) {
        5
    } else {
        9
    }
    // Set to 0 if empty
    if (TextUtils.isEmpty(this)) {
        return "0"
    }
    // add 0 before leading dot
    if (this.startsWith('.')) {
        return this.replaceFirst(".", "0.").formatNumber(maxInt, 2)
    }
    // skip formatting if entering a dot
    if (this.endsWith('.')) {
        return this
    }
    // Remove leading zeroes for numbers > 1
    if (this.toFloat() >= 1f) {
        return this.replaceFirst("^0+(?!$)".toRegex(), "").formatNumber(maxInt, 2)
    }
    // Remove extra zeroes for numbers between 0 and 1
    if (this.startsWith("00")) {
        return this.replaceFirst("^0(?!$)".toRegex(), "").formatNumber(maxInt, 2)
    }
    return this.formatNumber(maxInt, 2)
}

fun String.formatNumber(MAX_INT: Int, MAX_DECIMAL: Int): String {
    var str = BigDecimal(this).toPlainString()
    if (str[0] == '.') str = "0$str"
    var isDecimal = false
    var int = 0
    var decimal = 0

    var output = ""
    for (c in str) {
        if (c != '.' && !isDecimal) {
            int++
            if (int > MAX_INT) return output
        } else if (c == '.') {
            isDecimal = true
        } else {
            decimal++
            if (decimal > MAX_DECIMAL) return output
        }
        output += c
    }

    return output
}

/**
 * Text Color
 */

fun String.getCurrencyColor(context: Context) =
    if (this == "0") {
        ContextCompat.getColor(context, R.color.color_gray)
    } else {
        ContextCompat.getColor(context, R.color.color_black)
    }

/**
 * Activity
 */

fun AppCompatActivity.getRootView(): ViewGroup = findViewById(android.R.id.content)

fun AppCompatActivity.getActivityRoot(): View = getRootView().getChildAt(0)

fun Context.convertDpToPx(dp: Float) =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )

fun AppCompatActivity.hideKeyboard() =
    run { (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager) }
        .hideSoftInputFromWindow(currentFocus?.windowToken, 0)

fun AppCompatActivity.isKeyboardOpen(): Boolean {
    val visibleBounds = Rect()
    this.getActivityRoot().getWindowVisibleDisplayFrame(visibleBounds)

    val location = IntArray(2)
    getRootView().getLocationOnScreen(location)

    val screenHeight = getActivityRoot().rootView.height
    val heightDiff = screenHeight - visibleBounds.height() - location[1]
    val marginOfError = this.convertDpToPx(50F).roundToInt()
    return heightDiff > marginOfError
}
