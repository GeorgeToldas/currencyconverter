package com.georgetoldas.currencyconverter.utils

import android.text.InputFilter
import android.text.Spanned
import android.text.TextUtils
import java.util.regex.Matcher
import java.util.regex.Pattern

class DecimalDigitsInputFilter(
    digitsBeforeZero: Int = DIGITS_BEFORE_ZERO_DEFAULT,
    digitsAfterZero: Int = DIGITS_AFTER_ZERO_DEFAULT
) : InputFilter {
    private val pattern: Pattern = Pattern.compile(
        "-?[0-9]{0,$digitsBeforeZero}+((\\.[0-9]{0,$digitsAfterZero})?)|(\\.)?"
    )

    override fun filter(
        source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int
    ): CharSequence? {
        val replacement = source.subSequence(start, end).toString()
        val newVal = (dest.subSequence(0, dstart).toString() + replacement
                + dest.subSequence(dend, dest.length).toString())
        val matcher: Matcher = pattern.matcher(newVal)
        if (matcher.matches()) return null
        return if (TextUtils.isEmpty(source)) {
            dest.subSequence(dstart, dend)
        } else ""
    }

    companion object {
        private const val DIGITS_BEFORE_ZERO_DEFAULT = 5
        private const val DIGITS_AFTER_ZERO_DEFAULT = 2
    }

}