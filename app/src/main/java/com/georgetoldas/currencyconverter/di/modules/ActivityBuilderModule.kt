package com.georgetoldas.currencyconverter.di.modules

import com.georgetoldas.currencyconverter.di.annotations.ActivityScope
import com.georgetoldas.currencyconverter.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}