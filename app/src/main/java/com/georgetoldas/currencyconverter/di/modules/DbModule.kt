package com.georgetoldas.currencyconverter.di.modules

import android.content.Context
import androidx.room.Room
import com.georgetoldas.currencyconverter.BuildConfig
import com.georgetoldas.room.CurrencyDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideCurrencyDb(context: Context): CurrencyDb =
        Room.databaseBuilder(context, CurrencyDb::class.java, BuildConfig.CURRENCY_DB).build()
}