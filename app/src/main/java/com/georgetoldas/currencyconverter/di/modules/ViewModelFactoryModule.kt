package com.georgetoldas.currencyconverter.di.modules

import androidx.lifecycle.ViewModelProvider
import com.georgetoldas.currencyconverter.di.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * Provides an injectable ViewModel Factory
 */

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}