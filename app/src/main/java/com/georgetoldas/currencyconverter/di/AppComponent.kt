package com.georgetoldas.currencyconverter.di

import android.app.Activity
import android.app.Application
import com.georgetoldas.currencyconverter.CurrencyApp
import com.georgetoldas.currencyconverter.di.modules.ActivityBuilderModule
import com.georgetoldas.currencyconverter.di.modules.AppModule
import com.georgetoldas.currencyconverter.di.modules.DbModule
import com.georgetoldas.currencyconverter.di.modules.MainViewModelModule
import com.georgetoldas.network.di.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.DispatchingAndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules =
    [
        AndroidInjectionModule::class,
        ActivityBuilderModule::class,
        AppModule::class,
        DbModule::class,
        MainViewModelModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {

    fun inject(app: CurrencyApp)
    fun activityInjector(): DispatchingAndroidInjector<Activity>

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: Application): Builder

        @BindsInstance
        fun apiAddress(apiAddress: String): Builder

        fun build(): AppComponent
    }
}