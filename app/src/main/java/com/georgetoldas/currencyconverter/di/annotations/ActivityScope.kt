package com.georgetoldas.currencyconverter.di.annotations

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope