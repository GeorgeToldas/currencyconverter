package com.georgetoldas.currencyconverter.di.modules

import android.app.Application
import android.content.Context
import com.georgetoldas.currencyconverter.utils.CurrencyPrefs
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    fun provideCurrencyPrefs(context: Context) = CurrencyPrefs(context)
}