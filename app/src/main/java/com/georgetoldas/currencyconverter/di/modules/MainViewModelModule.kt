package com.georgetoldas.currencyconverter.di.modules

import androidx.lifecycle.ViewModel
import com.georgetoldas.currencyconverter.di.annotations.ViewModelKey
import com.georgetoldas.currencyconverter.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}