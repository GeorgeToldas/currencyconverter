package com.georgetoldas.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Currency::class], version = 1)
abstract class CurrencyDb : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}