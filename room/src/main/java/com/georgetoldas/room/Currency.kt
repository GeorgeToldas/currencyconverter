package com.georgetoldas.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currencies")
data class Currency(
    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "rate")
    var rate: Float,
    @ColumnInfo(name = "value")
    var value: Float,
    @ColumnInfo(name = "order")
    var order: Int
)