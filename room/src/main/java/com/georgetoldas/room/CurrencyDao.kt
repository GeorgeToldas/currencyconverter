package com.georgetoldas.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface CurrencyDao {

    @Query("SELECT * FROM currencies ORDER BY `order` ASC")
    fun getCurrencies(): List<Currency>

    @Query("DELETE FROM currencies")
    fun deleteAll()

    @Insert
    fun insertAll(currencies: List<Currency>)

    @Insert
    fun insert(currency: Currency)

    @Query("UPDATE currencies set rate= :rate, value= :value  WHERE name = :name")
    fun updateRates(name: String, rate: Float, value: Float)

    @Transaction
    fun updateRatesOffline(baseValue: Float) {
        val currencies = getCurrencies()
        currencies.map {
            if(baseValue != 0f) {
                it.rate = it.value / baseValue
                it.value = baseValue * it.rate
            }
        }
        updateList(currencies)
    }

    @Transaction
    fun updateValues(baseValue: Float) {
        val currencies = getCurrencies()
        currencies.map {
            if(baseValue != 0f) {
                if(it.order == 0) {
                    it.value = baseValue
                } else {
                    it.value = baseValue * it.rate
                }
            }
        }
        updateList(currencies)
    }

    @Transaction
    fun updateList(currencies: List<Currency>) {
        deleteAll()
        insertAll(currencies)
    }

}