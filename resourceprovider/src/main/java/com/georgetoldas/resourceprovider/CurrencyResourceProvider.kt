package com.georgetoldas.resourceprovider

interface CurrencyResourceProvider {
    fun getCurrencyFlag(currencyCode: String): Int
    fun getCurrencyName(currencyCode: String): Int
}