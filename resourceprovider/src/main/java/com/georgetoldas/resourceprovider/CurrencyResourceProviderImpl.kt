package com.georgetoldas.resourceprovider

import android.content.Context
import android.text.TextUtils

class CurrencyResourceProviderImpl(val context: Context) : CurrencyResourceProvider {

    override fun getCurrencyFlag(currencyCode: String) =
        getResource(currencyCode, FLAG_PREFIX, TYPE_DRAWABLE)

    override fun getCurrencyName(currencyCode: String) =
        getResource(currencyCode, CURRENCY_PREFIX, TYPE_STRING)

    private fun getResource(currencyCode: String, prefix: String, defType: String): Int {
        return if (!TextUtils.isEmpty(currencyCode)) {
            context.resources.getIdentifier(
                prefix + currencyCode.toLowerCase(),
                defType,
                context.packageName
            )
        } else { NO_ID }
    }

    companion object {
        private const val NO_ID = -1

        private const val FLAG_PREFIX = "ic_"
        private const val CURRENCY_PREFIX = "currency_"
        private const val TYPE_DRAWABLE = "drawable"
        private const val TYPE_STRING = "string"
    }
}