package com.georgetoldas.network.response

class ApiResponse {
    var baseCurrency: String = "EUR"
    var rates: HashMap<String, Float>? = null
}