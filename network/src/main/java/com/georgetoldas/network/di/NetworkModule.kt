package com.georgetoldas.network.di

import android.content.Context
import com.georgetoldas.network.ApiInterface
import com.georgetoldas.network.connection.ConnectivityLiveData
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(TIME_TO_CONNECT, TimeUnit.SECONDS)
            .readTimeout(TIME_TO_CONNECT, TimeUnit.SECONDS)
            .writeTimeout(TIME_TO_CONNECT, TimeUnit.SECONDS)
            .build()

    @Provides
    @Singleton
    fun provideApiInterface(apiAddress: String, okHttpClient: OkHttpClient): ApiInterface =
        Retrofit.Builder()
            .baseUrl(apiAddress)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build().create(ApiInterface::class.java)

    @Provides
    @Singleton
    fun provideConnectivityLiveData(context: Context) = ConnectivityLiveData(context)

    companion object {
        private const val TIME_TO_CONNECT = 30L
    }
}