package com.georgetoldas.network

import com.georgetoldas.network.response.ApiResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("latest")
    fun getCurrencies(@Query("base") baseCurrency: String): Observable<ApiResponse>
}